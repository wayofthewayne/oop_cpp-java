public class League{

	public static void print_table(Team teams[]){//will print the whole table 

		System.out.println("Club        MP     W     D     L     GF     GA     GD     Pts");
		System.out.println("-------------------------------------------------------------");

		for(int i =0; i<20; i++){
			System.out.format("%-12s%-7d%-6d%-6d%-6d%-7d%-7d%-7d%d\n",teams[i].tName,38,teams[i].wins,teams[i].draws,
			teams[i].losses,teams[i].goalsFor,teams[i].goalsAgainst,teams[i].goalDifference,
			teams[i].points);
		}
			
	}

	public static void main (String [] args){
           Team teams[] = new Team[20];

//team names 
         String names[] = {"Juventus","Inter","Roma","Napoli","Atalanta","Milan","Sassuolo","Sampdoria","Verona",
		"Lazio","Benevento","Fiorentina","Cagliari","Spezia","Genoa","Udinese","Parma","Bologna","Torino","Crotone"};


		for(int i=0; i<20; i++){//setting the names for the teams
			teams[i] = new Team(names[i]);
		}


		for(int i=0; i<19; i++){ //from the first team to the one before the last 
			for(int j=i+1; j<20; j++){ // start from the second, to the last 

				teams[i].Match(teams[j]); //play home and away for each combination 
				teams[j].Match(teams[i]);
			}
		}

		//sorting in descending order the Team objects according to the number of points they got.
		for (int i = 0; i < 20; i++){
			for (int j = i+1; j < 20; j++){
				if(teams[i].points <  teams[j].points){

					Team temp = new Team("temp"); // swapping 

					temp = teams[i];
					teams[i]=teams[j];
					teams[j]=temp;

				}
			}
		}

		//NOTE THIS SORT DOES NOT ALWAYS WORK

		/*re-sorting the list of objects such that those with the same number of points, are listed according to their goal
		 difference in descending order.*/

		int startFlag;//the first team with n number of points
		int lastFlag;//the last team with the same n number of points

		for (int i = 0; i<19;i++){ //note 19 cause of the i+1

			if(teams[i].points == teams[i+1].points){
				startFlag = i;

				//determining the lastFlag
				for(lastFlag = i+1; lastFlag<20; lastFlag++){

					if(lastFlag==19 || teams[lastFlag] != teams[lastFlag+1]){ //checking if points still are the same

						//lastFlag found!
						break;
					}
				}

				//sort all the teams from firstFlag to lastFlag in terms of their Goal difference.
				for (int x = startFlag; x <= lastFlag; x++){
					for (int y = x+1; y <= lastFlag ; y ++){
						if(teams[x].goalDifference <  teams[y].goalDifference){

							Team temp = new Team("temp");

							temp = teams[x];
							teams[x]=teams[y];
							teams[y]=temp;

						}
					}
				}

				i = lastFlag;//continue searching for teams with the same amount of points from lastFlag onwards

			}



		}


       //printing the final resulting table
		print_table(teams);



	}
}
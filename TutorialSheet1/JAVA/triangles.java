import java.util.Scanner;

public class triangles{

	public static int powerTo(int base, int exp){ //power function 
		int result =1;
		for (int i =0; i<exp;i++){
			result *= base;
		}

		return result;
	}

	public static boolean right_angle(int adj,int opp,int hyp){

		int rhs = powerTo(hyp,2); //squaring 
		int lhs =powerTo(adj,2)+powerTo(opp,2); //squaring and adding

		if(rhs == lhs){ //condition to check if right angled
			return true;
		}

		return false;
	}

	public static void main(String[] args){

		Scanner input = new Scanner(System.in);
		boolean rightAngle;

		int len1= input.nextInt();
		int len2=input.nextInt();
		int len3=input.nextInt(); //reading inputs

		if(len1>len2){ //finding longest side

			if(len1>len3){
				rightAngle=right_angle(len2,len3,len1);
			}
			else{
				rightAngle=right_angle(len1,len2,len3);
			}

		}
		else{

			if(len2 > len3){
				rightAngle=right_angle(len1,len3,len2);
			}
			else{
				rightAngle=right_angle(len1,len2,len3);
			}

		}

		if(rightAngle){ 
			System.out.println("The triangle is a right-angled Triangle!");
		}else{
			System.out.println("The triangle is not a right-angled Triangle!");
		}

	}
}

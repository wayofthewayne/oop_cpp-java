import java.util.Scanner; 


public class ScrabbleScore{
	public static void main (String [] args){
        
        Scanner scan = new Scanner (System.in);

        System.out.println("Enter a word"); 
        String word = scan.nextLine(); 

        int size = word.length(); //finding size of the word
        int score = 0, counter =0;

        for(int i=0; i<size; i++){
        	char c = word.charAt(i); //picking char by char 
        	counter = ScrabblePointSystem(c); //retreiving score for current char 
        	score += counter; //adding to total score 

        }

        System.out.println("Your score is : " + score); //outputting score 


	}

	static int ScrabblePointSystem(char c){

		c = Character.toUpperCase(c); //Converting to upper so we always match 

		switch(c){ //Scrabble code system 
			case 'E':
			case 'A':
			case 'I':
			case 'O': 
			case 'N':
			case 'R':
			case 'T':
			case 'L':
			case 'S':
			case 'U':
			return 1; 
			case 'D':
			case 'G':
			return 2;
			case 'B':
			case 'C':
			case 'M':
			case 'P':
			return 3;
			case 'F':
			case 'H':
			case 'V':
			case 'W':
			case 'Y':
			return 4;
			case 'K':
			return 5;
			case 'J':
			case 'X':
			return 8;
			case 'Q':
			case 'Z':
			return 10;
			default:
			return 0;
		}
	}
}
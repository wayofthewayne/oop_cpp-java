import java.util.Scanner;
import java.util.Random; 


public class Mastermind {
	public static void main (String [] args){

		  
          int randomPerm[] = new int[4];

          PermGen(randomPerm);

          String colours [] = new String [4];
          Scanner scan = new Scanner (System.in);
          int colourNo [] = new int[4];             

         int counter = 0; 

      do{
        
      	System.out.println("\n\nEnter 4 colours (order is imp, no rep), pressing enter after each one: "); //asking users to enter colours
      	System.out.println("The allowed colours are green,blue,red,yellow,black,brown");

          for(int i=0; i<4; i++){

          	colours[i] = scan.nextLine(); //reading colour
            colourNo[i] = Convertor(colours[i]); //conversion from string to int 


            if(colourNo[i] == -1){ //If colour is non existant, user is asked to re-enter
            	i--;
            	continue;
            }
          }

         System.out.println();


      	counter = 0; 

         for(int i=0; i<4; i++){
         	if(randomPerm[i] == colourNo[i]){ //if this condition is satisfied, the colour is in the correct position 
         		System.out.println("The colour " + colours[i] + " is in the sequence and in the correct position");
         		counter++;
         	}else{ // provided the colour is not in the correct position 
         		for(int j=0; j<4; j++){ // we run a loop comparing the current colour with the rest of the entered colour
         			if(randomPerm[i] == colourNo[j]){ //If this is satisfied it means it is in the sequence but not in the right position 
         				System.out.println("The colour " + colours[j] + " is in the sequence but entered in the wrong position");
         			}
         		}
         	}
         }
                 

        }while(counter<4); //keeping track of amount of colours, in the right position 
             

        System.out.println("\n\nCongrats you have won the game!");
         
          
    }

    static void PermGen(int perm[]){

    	int upperLimit = 6;
        Random rand = new Random();

    	int uniqueCheck= 0; 
        boolean nonUnqiue = false;
   

    	for(int i=0; i<4; i++){

             uniqueCheck = rand.nextInt(upperLimit); //generating a random no between 0-5

             for(int j=0; j<i; j++){

             	if(perm[j] == uniqueCheck){ //comparing with elements already in the array 
             		nonUnqiue = true; //triggering bool 
             		break;
             	}
             }

             if(nonUnqiue == true){ //if bool triggered
             	i--; // reset i 
             	nonUnqiue = false; //reset bool
             	continue; //skip rest of iteration
             }


          	perm[i] = uniqueCheck; //if it is unique store in array 
          }

    }

    static int Convertor(String colour){
        colour = colour.toLowerCase();

    	switch(colour){
    		case "red": 
    		 return 0;

    		case "green":
    		 return 1;

    		case "yellow":
    		 return 2;

    		case "black":
    		 return 3;

    		case "blue":
    		 return 4;

    		case "brown":
    		 return 5;

    		default:
    		 System.out.println("This colour is not part of the game. Re-enter:\n");
    		 return -1;

    	}
    }
}
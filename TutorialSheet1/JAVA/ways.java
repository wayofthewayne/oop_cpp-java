public class ways{

	public static int combs(int num,int upperLim){ // for btr comments find same program of cpp 
		
		int ways = 0;// intialising ways to 0

		if(num >= 200 && 200<= upperLim){ //if total number is bigger than 200, then we can take 200 away from
			ways =ways + combs(num-200,200);
		}

		if(num >= 100 && 100 <= upperLim){// same as above but for 100
			ways =ways + combs(num-100,100);
		}

		if(num>= 50 && 50<= upperLim){
			ways = ways + combs(num-50,50);
		}

		if(num>= 20 && 20<= upperLim){
			ways = ways + combs(num-20,20);
			
			
		}

		if(num>= 10&& 10<= upperLim){
			ways = ways + combs(num-10,10);
		}

		if(num>= 5 && 5<= upperLim){
			ways = ways + combs(num-5,5);
		}

		if(num>= 2 && 2<= upperLim){
			ways = ways + combs(num-2,2);
		}

		if(num>= 1 && 1<= upperLim){
			ways = ways + combs(num-1,1);
		}



		if(num==0){//if number reached 0; it means we reached the last number of a specific combination therefore return 1
			return 1;
		}

		//else

		return ways;
		

	}


	public static void main(String[] args){

		int ways = combs(500,500);

		System.out.println(ways);

	}
}

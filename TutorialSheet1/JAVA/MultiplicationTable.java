

public class MultiplicationTable{
public static void main (String [] args){
  
  if(args.length == 0){ // note length (without the brackets) is used to read the size of the array 
  	System.out.println("No commands were entered\n");
  	System.exit(0); 
  }

  Multiplication(args);
}


static void Multiplication(String [] args){

    int num; 
    boolean delimCheck = false;

    for(int i=0; i<args.length; i++){

    	String arg = args[i]; //processing each string one by one 
    	delimCheck = false; 

    	for(int j=0; j<args[i].length(); j++){ //note how here we do use the bracket 


           if(arg.charAt(j) == '-'){ //checking for delim 
           	delimCheck = true; 

           	String firstNum = args[i].substring(0,j); //storing the string without the dash 
           	String lastNum = args[i].substring(j+1,args[i].length()); // storing the rest of the rest of the string 


           	int baseNum = Integer.parseInt(firstNum); //converting string to int 
           	int limNum = Integer.parseInt(lastNum); 

           	MultiplicationRange(baseNum,limNum);
           	break;  
           }
    	}

    	if(delimCheck == false){
    		num = Integer.parseInt(args[i]);
    		MultiTable(num);
    	}
    }

}



static void MultiTable(int num){ //will calculate the mutlipication till 12 for that spefic number 
	for(int i=0; i<13; i++){
		System.out.println(num+" * "+i+" = "+num*i); 
	}
	System.out.println("");
}

static void MultiplicationRange(int bottom, int top){ // will calculate the mutliplication table for the given range 

	do{
	for(int i=0; i<13; i++){
		System.out.println(bottom+" * "+i+" = "+bottom*i); 
	}
	    bottom++;
	    System.out.println("");
    }while(bottom <= top);
}
}
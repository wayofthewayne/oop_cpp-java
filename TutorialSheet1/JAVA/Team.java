import java.util.Random;


public class Team{

 
	String tName; //team attributes set to public so they can be accessed outside the class
	int points; 
	int matchesPlayed;
	int goalsFor;
	int goalsAgainst; 
	int goalDifference; 
	int wins;
	int losses;
	int draws; 


//A constructor 
	public Team(String name){ //sets the team name 
		tName = name; 
	}

//Will simulate a match 
	public void Match(Team away){
         Random rand = new Random();

		int homeScore = rand.nextInt(6); //generate a random score from 0 to 5
		int awayScore = rand.nextInt(6); 

		goalsFor += homeScore;  //goals for home
		goalsAgainst += awayScore; 
		goalDifference = goalsFor - goalsAgainst;

		away.goalsFor += awayScore; //goals for away
		away.goalsAgainst += homeScore; 
		away.goalDifference = away.goalsFor - away.goalsAgainst;

		if(homeScore > awayScore){ // wins and losses 
			wins++;
			away.losses++;
			points+=3;

		}else if (awayScore > homeScore){
			losses++;
			away.wins++;
			away.points+=3; 
			
		}else{ //if draw 
			draws++;
			away.draws++;
			points++;
			away.points++; 
		}

	}
}
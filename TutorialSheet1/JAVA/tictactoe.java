import java.util.Scanner;
import java.util.Random;


public class tictactoe{

	//function to display board
	public static void display(String board[]){
		System.out.println("+---+---+---+");
		System.out.println("| "+board[0]+" | "+board[1]+" | "+board[2]+" |");
		System.out.println("+---+---+---+");
		System.out.println("| "+board[3]+" | "+board[4]+" | "+board[5]+" |");
		System.out.println("+---+---+---+");
		System.out.println("| "+board[6]+" | "+board[7]+" | "+board[8]+" |");
		System.out.println("+---+---+---+");

	}


	//function which checks if any row suffies winning condition
	public static boolean checkRows(String board[], String token){// token is either x or o depending on who's turn it is
		if(board[0]==board[1] && board[1]==board[2] && board[2]==token){
			return true;
		}
		if(board[3]==board[4] && board[4]==board[5] && board[5]==token){
			return true;
		}
		if(board[6]==board[7] && board[7]==board[8] && board[8]==token){
			return true;
		}
		return false;
	}


	//function which checks if any column suffies winning condition
	public static boolean checkColumns(String board[], String token){
		if(board[0]==board[3] && board[3]==board[6] && board[6]==token){
			return true;
		}
		if(board[1]==board[4] && board[4]==board[7] && board[7]==token){
			return true;
		}
		if(board[2]==board[5] && board[5]==board[8] && board[8]==token){
			return true;
		}
		return false;
	}


	//function which checks if any diagonal suffies winning condition
	public static boolean checkDiagonals(String board[], String token){
		if(board[0]==board[4] && board[4]==board[8] && board[8]==token){
			return true;
		}
		if(board[2]==board[4] && board[4]==board[6] && board[6]==token){
			return true;
		}
		return false;
	}



	//function which checks for any possible winning condition (row,column or diagonal) 
	public static boolean check(String board[], String token){
		return(checkRows(board,token) || checkDiagonals(board,token) || checkColumns(board,token));
	}


	//2 player version
	public static void co_op(){

		String board[] = {" "," "," "," "," "," "," "," "," "};// playing board
		String board2[]= {"0","1","2","3","4","5","6","7","8"};//board used to display what each number corresponds to

		System.out.println("enter player1's name:");

			Scanner input = new Scanner(System.in);

			String player1 = input.nextLine();

			System.out.println("enter player2's name:");

			String player2 = input.nextLine();

			int pos;

			int turnCounter=0;

			while(true){
				System.out.println(player1+"'s turn!");

				display(board2);

				System.out.println(player1+" enter the position you want to play in: ");
				pos = input.nextInt();

				while(true){//making sure position of the number entered by user is not already taken
					if(board[pos]==" "){
						board2[pos]=" ";
						board[pos]="X";
						break;
					}
				
					System.out.println("incorrect position, try again:");
				
					pos = input.nextInt();
				}

				if(check(board,"X")){//check winning condition with token = X
					display(board);//if true player 1 won
					System.out.println(player1+" won the game!");
					break;
				}

				display(board);

				turnCounter++;

				if(turnCounter==9){
					System.out.println("DRAW!");
					break;
				}
				//same algorithm for player 2
				System.out.println(player2+"'s turn!");

				display(board2);

				System.out.println(player2+" enter the position you want to play in: ");
				pos = input.nextInt();

				while(true){
					if(board[pos]==" "){
						board2[pos]=" ";
						board[pos]="O";
						break;
					}
				
					System.out.println("incorrect position, try again:");
				
					pos = input.nextInt();
				}

				if(check(board,"O")){
					display(board);
					System.out.println(player2+" won the game!");
					break;
				}

				display(board);

				turnCounter++;

			}
	}

	//function for single player
	public static void single_player(){
		String board[] = {" "," "," "," "," "," "," "," "," "};
		String board2[]= {"0","1","2","3","4","5","6","7","8"};
		
		System.out.println("enter player1's name:");

			Scanner input = new Scanner(System.in);

			String player1 = input.nextLine();

			int pos;

			int turnCounter=0;

			while(true){
				System.out.println(player1+"'s turn!");

				display(board2);

				System.out.println(player1+" enter the position you want to play in: ");
				pos = input.nextInt();
				while(true){
					if(board[pos]==" "){
						board2[pos]=" ";
						board[pos]="X";
						break;
					}
				
					System.out.println("incorrect position, try again:");
				
					pos = input.nextInt();
				}

				if(check(board,"X")){
					display(board);
					System.out.println(player1+" won the game!");
					break;
				}

				display(board);

				turnCounter++;

				if(turnCounter==9){
					System.out.println("DRAW!");
					break;
				}

				Random rand = new Random();

				//choose random number from 0-8
				pos = rand.nextInt(9);

				


								
				while(true){
					//if pos chosen by computer does not corresponds to " ", then computer shall re-choose
					if(board[pos]==" "){
						board2[pos]=" ";
						board[pos]="O";
						break;
					}
				
				
					pos = rand.nextInt(9);
				}

				if(check(board,"O")){
					display(board);
					System.out.println("Computer won the game!");
					break;
				}

				display(board);

				turnCounter++;

			}
	}







	public static void main(String[] args){
		System.out.println("WELCOME TO TICTACTOE!");

		System.out.println("choose 1 to play single player, choose 2 for 2-player (3 to quit).");
		Scanner input = new Scanner(System.in);

		int opt = input.nextInt();

		while(opt < 3){
			if(opt == 1){
				single_player();
			}else{
				co_op();
			}
			System.out.println("choose 1 to play single player, choose 2 for 2-player (3 to quit).");
			opt = input.nextInt();	
		}	




		


	}
}

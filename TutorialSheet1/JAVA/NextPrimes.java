import java.util.Scanner;


public class NextPrimes{
	public static void main (String [] args){
        
    Scanner scan = new Scanner (System.in); 

    System.out.println("Enter a number"); //reading input 
    int number = scan.nextInt(); 

    int primeCounter=0; 
    boolean check = false; 

    do {
    	check = false;
    	check = isPrime(number); //checking if the number is prime 

    	if(check == true){
    	   System.out.println(number);  //outputting prime number 
           primeCounter++; //incrementing amount of primes found 
           number++; //moving onto next number

    	}else{
    		number++; //moving onto next number 
    	}
    }while(primeCounter < 3); //repeating until 3 primes are found

	}


	static boolean isPrime(int n){

	int flag =0; 

	for (int i = 2; i <= (n / 2); i++) {

        // condition for non-prime
        if (n % i == 0) { //checking if divisible
            flag = 1;
            break;
        }
    }

    if (n == 1) {
        return false;
    }else {
        if (flag == 0){
            return true; //meaning prime 
        }
        else{
            return false; 
        }
    }

}
}


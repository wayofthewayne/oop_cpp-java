import java.util.Scanner;
import java.util.Random;


public class RockPaperScissors{
	public static void main (String [] args){
            
            Scanner scan = new Scanner (System.in);

            String inputP1;
            String inputP2;

            int scoreP1 =0;
            int scoreP2 =0; 
            int winner = 0; 


            do{


            System.out.println("Player 1 turn: "); //reading player 1 input
            inputP1 = scan.nextLine();

            inputP1 = inputP1.toUpperCase(); 

            if(inputP1.equals("ROCK") == false && inputP1.equals("PAPER") == false && inputP1.equals("SCISSORS")==false){
            	System.out.println("This command was not recognized\n"); //checking if input is valid
            	continue;
            }

            System.out.println("CPU s turn: "); //reading and displaying cpu input
            inputP2 = CPUTurn();
            System.out.print(inputP2+ "\n\n");

            

            winner = Win(inputP1,inputP2); //checking who round

            if(winner == 1){ //depending on the winner, the right score is incremented 
              scoreP1++;
            }

            if(winner == 2){
            	scoreP2++;
            }

            System.out.println("Score: "+ scoreP1 + "-" + scoreP2); //score is displayed

        }while(scoreP1 < 3 && scoreP2 < 3); //repeated until first player reaches a score of three

        if(scoreP1 == 3){ //displaying final winner
        	System.out.println("Player 1 Wins");
        }else{
        	System.out.println("CPU Wins");
        }

	}

	static String CPUTurn(){ //this function will generate the CPU s turn 
       Random rand = new Random(); 
       int upperbound = 3; 


		   String input= "";
           int option = rand.nextInt(upperbound);//generating a random number between 0-2

           switch(option){//each number has a corresponding string value

           	case 0: 
           	input = "rock";
           	break;

           	case 1: 
           	input = "paper";
           	break;

           	case 2:
           	input = "Scissors";
           	break;
           }

           return input; //returing the value
	}

	static int Win(String p1, String p2){
         p1 = p1.toUpperCase(); //converting to upper case to avoid case sensitivity
         p2 = p2.toUpperCase();

        if(p1.equals(p2) == true){ // all draw cases
        	System.out.println("It's a draw!");
        	return 0; 
        }

        if(p1.equals("ROCK") && p2.equals("SCISSORS")){//Outcomes when player 1 chooses rock
        	System.out.println("Player 1 wins"); 
        	return 1;
        }

        if(p1.equals("ROCK") && p2.equals("PAPER")){
        	System.out.println("Player 2 wins"); 
        	return 2;
        }

        if(p1.equals("PAPER") && p2.equals("SCISSORS")){//outcomes when player 1 chooses paper
        	System.out.println("Player 2 wins"); 
        	return 2;
        }

        if(p1.equals("PAPER") && p2.equals("ROCK")){
        	System.out.println("Player 1 wins"); 
        	return 1;
        }

        if(p1.equals("SCISSORS") && p2.equals("ROCK")){//Outcome when player 1 chooses scissors
        	System.out.println("Player 2 wins"); 
        	return 2;
        } 

        if(p1.equals("SCISSORS") && p2.equals("PAPER")){
        	System.out.println("Player 1 wins"); 
        	return 1;
        } 

        return 0; //each return value acts as a indiction of which player won 
        //a return of 0 implies a draw, a return of 1 implies player 1 won, a return of 2 implies CPU won 
       

	}

}
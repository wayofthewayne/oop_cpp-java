import java.util.Scanner; //Importing scanner class


public class StringReverse{
	public static void main (String [] args){

	Scanner myObj = new Scanner(System.in); //Creating scanner obj 

	System.out.println("Enter a string"); 
	String sentence = myObj.nextLine(); // reading input in scanner obj 

    ReverseString(sentence);


	}


	static void ReverseString(String line){
          int stringLength = line.length(); // retreiving string length 
          
          for(int i=stringLength-1; i>-1; i--){
          	System.out.print(line.charAt(i)); //processing characters from back 
          }
	}
}
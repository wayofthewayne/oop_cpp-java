import java.util.Random; 

//***********************THIS IS NOT A PROPER WORKING SOLUTION*******************
public class Maze{

	public static void main(String [] args){
	     char maze[][] = new char [10][10]; //maze to be stored
	     boolean visited[][] = new boolean [10][10]; //an array determining which parts of the array were visited 
	     //initially all set to false 


	     for(int i=0; i<10; i++){ //setting the boolean array to all false. 
	     	for(int j=0; j<10; j++){ //should be redudant as this is done by default 
	     		visited[i][j] = false;
	     	}
	     }

	     boolean path = false; 

	     Border(maze); // genetaring the maze 

	     for(int i=0; i<10; i++){ //displaying maze 
	     	for(int j=0; j<10; j++){
               System.out.print(maze[i][j]);
	     	}	  
	     	System.out.println();
	     }
         


         int x=0,y=0;
         boolean leaveLoops = false;

	     for(int i=0; i<10; i++){// checking for existing path 
	     	for(int j=0; j<10; j++){
	     		if(maze[i][j] == '>') // getting the starting position 
	     		    maze[i][j] = '*'; // setting it as wall
	     			x =j;
	     			y =i;   

	     			leaveLoops = true; //so we can break from outer loop 

	     			break;
	     	}

	     	if(leaveLoops == true){
	     		break;
	     	}
	     }

	     path = Traverse(maze,visited,x,y);

	    if(path == true){
	    	System.out.println("\n\nPath does exist.\n");
	    }else{
	    	System.out.println("\n\nPath does not exist.\n");
	    }
	}

    public static void Border(char Arr[][]){

       
       for(int i=0; i<10; i++){ //populating the empty array with spaces 
       	for(int j=0; j<10; j++){
       		Arr[i][j] = ' ';
       	 }
       }
           

           for(int i=0; i<10; i++){ //setting the edges 
           	  Arr[0][i] = '*';
           	  Arr[i][0] = '*';
           	  Arr[9][i] = '*'; 
           	  Arr[i][9] = '*';
           }

          Random randNo = new Random(); 
          

          int optionEntrance = randNo.nextInt(4); //generating random no between 0-3
          int positionEntrance = randNo.nextInt(10); //generating a random no between 0-9 


          switch (optionEntrance){ //to place an entrance somewhere randomly along the border 
          	case 0: 
          	Arr[0][positionEntrance] = '>';
          	break; 
          	case 1:
          	Arr[positionEntrance][0] = '>'; 
          	break; 
          	case 2:
          	Arr[9][positionEntrance] = '>';
          	break; 
          	case 3:
          	Arr[positionEntrance][9] = '>';
          	break;
          	default:
          	System.out.println("Error entrance");
         }

         int optionExit;
         int positionExit;

         do{

          optionExit = randNo.nextInt(4); //generating random no between 0-3
          positionExit = randNo.nextInt(10); //generating a random no between 0-9 

          if(optionExit == optionEntrance && positionExit == positionEntrance){
                 continue;
          }//so we do not set the exit anyway.      

          switch (optionExit){ //to place an exit somewhere randomly along the border 
          	case 0: 
          	Arr[0][positionExit] = '<';
          	break; 
          	case 1:
          	Arr[positionExit][0] = '<'; 
          	break; 
          	case 2:
          	Arr[9][positionExit] = '<';
          	break; 
          	case 3:
          	Arr[positionExit][9] = '<';
          	break;
          	default:
          	System.out.println("error Exit");
         }

         

     }while(optionExit == optionEntrance && positionExit == positionEntrance); //making sure exit and entrance are not the same place 
         

         int fillOrNo;  

         for(int i=1; i<9; i++){ //filling the middle of the maze randomly 
         	for(int j=1; j<9; j++){
                
                fillOrNo = randNo.nextInt(3); //generating a random no between(0-2)
                
                //the ration wall:space is 1:2
         		if(fillOrNo == 1){
         			Arr[i][j] = '*';
         		}
         	}
         }


    }


    public static boolean Traverse(char Arr[][], boolean visited[][], int xAxis, int yAxis){

    visited[yAxis][xAxis] = true; //setting the current position to visited 

    boolean path1 = false, path2 = false, path3 = false, path4 = false;

    if(yAxis<9){ //note yAxis is never 9(*) since if it is it s on the edge and can t move out anyways (prevents out of bounds)
    	//if yAxis is below 9 then we can move upwards 
    	// * Note it may be on edge in case of starting position, however, going up will not be an option, this would result in out of bounds error

    	if(Arr[yAxis+1][xAxis] != '*' && visited[yAxis+1][xAxis] != true){ //checking for wall of if already visited 
    		if(Arr[yAxis+1][xAxis] == '<'){ //checking if exit is found 
    			return true; 
    		}
               
            path1 = Traverse(Arr, visited, xAxis, yAxis+1); //continuing on path 

    	}
    }

    if(yAxis>0){ //same logic to go downwards 

    	if(Arr[yAxis-1][xAxis] != '*' && visited[yAxis-1][xAxis] != true){
              if(Arr[yAxis-1][xAxis] == '<'){
              	return true;
              }

              path2 = Traverse(Arr,visited, xAxis, yAxis-1);
    	}
    }


    if(xAxis>0){ //same logic to go left 

    	if(Arr[yAxis][xAxis-1] != '*' && visited[yAxis][xAxis-1] != true){
              if(Arr[yAxis][xAxis-1] == '<'){
              	return true;
              }

              path3 = Traverse(Arr,visited, xAxis-1, yAxis);
    	}
    }
 
    if(xAxis<9){ //same logic to go right 

    	if(Arr[yAxis][xAxis+1] != '*' && visited[yAxis][xAxis+1] != true){
              if(Arr[yAxis][xAxis+1] == '<'){
              	return true;
              }

              path4 = Traverse(Arr,visited, xAxis+1, yAxis);
    	}
    }
     

    if(path1 == true || path2 == true || path3 == true || path4 == true){
    	return true;
    }else{
    	return false;
    }

   }
 }

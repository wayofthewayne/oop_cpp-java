import java.util.Scanner; 

public class WordHistogram{
	public static void main (String args[]){
 
    Scanner scan = new Scanner (System.in);

    String words; 

	 System.out.println("Enter your word/sentence/paragraph");
     words = scan.nextLine(); //reading user input

     boolean alreadyCounted = false; 
     int counter = 0; 

     for(int i=0; i<words.length(); i++){

     	for(int j=0;j<i; j++){ 
     		if(words.charAt(i) == words.charAt(j)){ //will compare current index to previous characters 
     			alreadyCounted = true; //If it was considered we break out and then skip the loop 
     			break; //break is here for efficiency 
     		}
     	}

     	if(alreadyCounted == true){ //skipping loop if current character was already considered 
     		alreadyCounted = false; 
     		continue;
     	}
        
        for(int j=0; j<words.length(); j++){ //running a loop from the beginning comparing to current index 
     	  if(words.charAt(i) == words.charAt(j)){
            counter++; 
     	  }
       } 

     	Print(counter,words.charAt(i)); //Function call to print *
     	counter=0; 


     }
	}

	static void Print(int counter, char c){ //will print the appropriate amount of *

		System.out.print(c + ": ");
           for(int i=0; i<counter; i++){
           	System.out.print("*");
           }

           System.out.println("\n");
	}
}
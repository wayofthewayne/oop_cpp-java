#include <iostream>
#include <stdbool.h>
using namespace std;

bool isPrime(int num){
	//function to determine whether a given number is prime or not
	for(int i = 2; i < num/2; i++){
		if (num%i==0){
			return false;
		}
	}
	return true;
}

void next3Primes(int num){

	int primeCounter=0;//number of primes displayed.

	while(primeCounter<3){//stop when 3 prime numbers are displayed.

		num++;

		if(isPrime(num)){//if number is prime, display it and increment counter.
			cout << num << " is a prime number" << endl;
			primeCounter++;
		}
	}
}

int main(void){

	int num;
	cout << "Enter any integer."<< endl;

	cin >> num;

	next3Primes(num);



	return 0;	
}

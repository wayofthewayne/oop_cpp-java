#include <iostream>
#include <string>
#include <stdbool.h>
#include <time.h>
using namespace std;

bool search_path(string realMaze[],string flagMaze[],int y, int x);

int main(void){

	string maze[20];// randomly generated, with the exception of a fixed border and 2 openings (start and finish)

	string maze2[20];//these two are test cases 
	string maze3[20];

	//maze2 and maze3 are not part of the implementation, they're created for testing purposes only 

	//maze2 is created with a an existing path from start to end
	maze2[0] ="X XXXXXXXXXXXXXXXXXX";
	maze2[1] ="X XXXXXXXXX     XXXX";
	maze2[2] ="X    XXX    XXX XXXX";
	maze2[3] ="XXXX XXX XXXXXX XXXX";
	maze2[4] ="XXXX XXX XXXXXX XXXX";
	maze2[5] ="XXXX            XXXX";
	maze2[6] ="XXXX XXXXXXXXXXXXXXX";
	maze2[7] ="XXXX XXXXXXXXXXXXXXX";
	maze2[8] ="XXXX XXXXXXXXXXXXXXX";
	maze2[9] ="XXXX XXXXXXXXXXXXXXX";
	maze2[10] ="XXXX              XX";
	maze2[11] ="XXXX XXXXXXXXXXXXXXX";
	maze2[12] ="XXXX XXXXXXXXXXXXXXX";
	maze2[13] ="XXXX XXXXXXXXXXXXXXX";
	maze2[14] ="XXXX XXXXXXXXXXXXXXX";
	maze2[15] ="XXXX XXXXXXXXXXXXXXX";
	maze2[16] ="XXXX xXXXXXXXXXXXXXX";
	maze2[17] ="XXXX              XX";
	maze2[18] ="XXXX XXXXXXXXXXXX  X";
	maze2[19] ="XXXXXXXXXXXXXXXXXX X";

	//maze3 is created with no paths from start to end
	maze3[0] ="X XXXXXXXXXXXXXXXXXX";
	maze3[1] ="X XXXXXXXXX     XXXX";
	maze3[2] ="X    XXX    XXX XXXX";
	maze3[3] ="XXXX XXX XXXXXX XXXX";
	maze3[4] ="XXXX XXX XXXXXX XXXX";
 	maze3[5] ="XXXX            XXXX";
	maze3[6] ="XXXX XXXXXXXXXXXXXXX";
	maze3[7] ="XXXX XXXXXXXXXXXXXXX";
	maze3[8] ="XXXX XXXXXXXXXXXXXXX";
	maze3[9] ="XXXX XXXXXXXXXXXXXXX";
	maze3[10] ="XXXX              XX";
	maze3[11] ="XXXX XXXXXXXXXXXXXXX";
	maze3[12] ="XXXX XXXXXXXXXXXXXXX";
	maze3[13] ="XXXX XXXXXXXXXXXXXXX";
	maze3[14] ="XXXX XXXXXXXXXXXXXXX";
	maze3[15] ="XXXX XXXXXXXXXXXXXXX";
	maze3[16] ="XXXX xXXXXXXXXXXXXXX";
	maze3[17] ="XXXX              XX";
	maze3[18] ="XXXX XXXXXXXXXXXX XX";
	maze3[19] ="XXXXXXXXXXXXXXXXXX X";
	

	string flagMaze[20];// a 2d array, where each of the 400 characters will be a whitespace character.
	//this 'maze' will be used to flag the path already explored in the search_path algorithm.
	//this is done to avoid looping around in a certain path which would result in an infinite loop.




	int random; //stores the randomly generated number (0-2)



	for(int i =0;i<20;i++){
		flagMaze[i]="                    ";//assigning the whitespace characters to the flagMaze
	}



	for(int i =0;i<20;i++){
		maze[i]="XXXXXXXXXXXXXXXXXXXX";// filling the maze with 'X's in order to define its size.
	}

	maze[0][1]=' ';//this is the staring point, therefore should be whitespace
	maze[19][18]=' ';//this is the end point


	


	srand(time(0));//setting the seed

	//algorithm which randomly changes certain 'X' characters to whitespace (creating paths randomly)
	//note that the border is fixed and not changed.(hence why both loops run from (i/j =  0) to (i/j = 18))
	for(int i =1;i<19;i++){

		for (int j =1 ;j<19;j++){
			random = rand()%3;
			if(random==1 || random == 0){// if rand returns 0 or 1, that char will be changed. 
				//We should expect the maze to contain more whitespace than 'X' in order to increase the chances of a path existing

				maze[i][j]=' ';

			}
			
		}
	}


	//printing the maze
	for(int i =0;i<20;i++){
		cout << maze[i]<<endl;
		
	}

	if(search_path(maze,flagMaze,0,1)){
		cout<<"A path exists!"<<endl;
	}else{
		cout <<"There is no path from start to finish"<<endl;
	}

	return 0;
}


bool search_path(string realMaze[],string flagMaze[],int y, int x){



	flagMaze[y][x]='X';//flagging the current position since it is now visited

	bool path1=false,path2=false,path3=false,path4=false;//4 paths (up, down, left, right)

	if(y<19){// if y  (which indicates the vertical positioning in the 2d array) is less than 19,
	// then there exists a position below it in the maze (i.e y+1) 

		if(flagMaze[y+1][x]==' ' && realMaze[y+1][x]==' '){

			if(y+1 == 19 && x == 18){//if this new position is the finish line, return true
				return true;
			}

			//else - visit this new position
			path1 = search_path(realMaze,flagMaze,y+1,x);
		}

	}

	if(y>0){// same logic as above but for a vertical upwards movement.

		if(flagMaze[y-1][x]==' ' && realMaze[y-1][x]==' '){

			
			path2 = search_path(realMaze,flagMaze,y-1,x);
		}

	}

	if(x>0){// same logic as above but for a right horizontal movement.

		if(flagMaze[y][x-1]==' ' && realMaze[y][x-1]==' '){

			
			path3 = search_path(realMaze,flagMaze,y,x-1);
		}

	}

	if(x<20){// same logic as above but for an left horizontal movement.

		if(flagMaze[y][x+1]==' ' && realMaze[y][x+1]==' '){
			
			
			path4 = search_path(realMaze,flagMaze,y,x+1);
		}

	}

	//if either of the 4 paths resulted in a succesful path(i.e. true), then return true
	return path1 || path2 || path3 || path4;
}

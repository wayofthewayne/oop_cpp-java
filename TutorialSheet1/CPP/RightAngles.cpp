#include <iostream>
using namespace std; 


bool RightAngle(double hyp, double side, double side1);

int main (void){

	double side1,side2,side3;

    cout << "Enter the three sides of the triangle"<<endl; //Entering and storing input
    cin >> side1;
    cin >> side2;
    cin >> side3;

    if(side1 > side2 && side1>side3){ //determining which is the longest side. This is passed to function as the hyp parameter
          if(RightAngle(side1,side2,side3)==true){
          	cout<<"This is a right angled triangle"<<endl;
          	return 0;
          }
    }else if(side2 > side1 && side2 > side3){
           if(RightAngle(side2,side1,side3)==true){
          	cout<<"This is a right angled triangle"<<endl;
          	return 0;
          }
    }else if(side3 > side2 && side3 > side1){
           if(RightAngle(side3,side2,side1)==true){
          	cout<<"This is a right angled triangle"<<endl;
          	return 0;
          }
    }else{ //reaching this case implies there is no maximum (i.e all sides are equal)
    	cout << "All sides are equal. This is an equilateral"<<endl;
    	return 0;
    }

    cout<<"This is not a right angled triangle"<<endl; //If non of the if conditions were triggered then we can conclude it was not a right angled triangle
	return 0; 
}

bool RightAngle(double hyp, double side, double side1){ //algorithm to determine if it is a right angled triangle

	double squareSide = side*side; //squaring short sides
	double squareSide1 = side1*side1; 

	double total = squareSide1 + squareSide; //adding their squares

    double squareHyp = hyp*hyp; //square the longest side

    if(squareHyp == total){ //if it is equal to their sum, then it is a right angled triangle 
    	return true;
    }

    return false; //else it is not 
}
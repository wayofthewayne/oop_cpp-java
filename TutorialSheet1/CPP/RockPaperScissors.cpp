#include <iostream>
#include <stdlib.h>
#include <stdbool.h>
using namespace std;

int main(void){


	cout << "welcome to Rock paper and scissors!" << endl;
	int userOpt,userScore = 0;
	int compOpt, compScore = 0;
	int outcome; //1 means comp win 2 means user win 3 means draw
	
	while (true){

		cout<<"press 1 to play rock\npress 2 to play paper\npress 3 to play paper scissors." << endl;
		cin >> userOpt;

		while (userOpt < 1 || userOpt > 3){ //checking for valid entry 
			cout << "wrong entry. try again"<<endl;
			cin >> userOpt;
		}

		compOpt = rand()%3 + 1;//random number generator between 1-3

		switch(compOpt){ //will compute outcome depending on computer choice 

			case 1:

				cout << "computer chose rock"<<endl;
				if(userOpt == 1){
					outcome = 3;
				}else if(userOpt == 2){
					outcome = 2;
				}else{
					outcome = 1;
				}

				break;

			case 2:
				cout << "computer chose paper"<<endl;
				if(userOpt == 1){
					outcome = 1;
				}else if(userOpt == 2){
					outcome = 3;
				}else{
					outcome = 2;
				}

				break;

			case 3:
				cout << "computer chose scissors"<<endl;
				if(userOpt == 1){
					outcome = 2;
				}else if(userOpt == 2){
					outcome = 1;
				}else{
					outcome = 3;
				}

				break;

			default:
				break;
		}

		if (outcome == 1){ //compuiting round outcome 
			compScore++;
			cout << "Computer won the round!"<<endl;

		}else if(outcome == 2){
			userScore++;
			cout << "User won the round!" << endl;

		}else{
			cout << "Draw!" << endl;
		}

		cout << "Player:  User    Computer\nScore:    "<< userScore << "         "<< compScore << endl;

		if(userScore == 40){ //end game winner 
			cout << "User won the game!" <<endl;
			break;
		}else if(compScore == 40){
			cout << "Computer won the game!"<< endl;
			break;
		}




	}

	return 0;
}

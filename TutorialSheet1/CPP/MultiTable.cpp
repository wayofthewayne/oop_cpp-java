#include <iostream>
using namespace std; 

#include <string.h>

void MultiRange(int start, int end);
void MultiTable(int num);

int main (int argc, char **argv){

	if(argc == 1){ //Checking commands are being passed
		cout <<"No commands entered!"<<endl;
	}

	

    const char* delim = "-\0"; // We use const due to the way the strstr function is defined 

	for(int i=1; i<argc; i++){ //We start from 1 so we don t include the name of the program 

		char *string1 = argv[i]; // checking string by string

		char *returnV = strstr(string1,delim); //checking for range indication 
         if( returnV != NULL){ //If we find the delim 


           
           size_t originalSize = strlen(string1);
           size_t delimOnwards = strlen(returnV); 
           size_t pos = originalSize - delimOnwards; //find position of the delim 
            

           char no1[pos+1]= {'\0'}; // we need to add 1 for array to store everything (0 bound index)
           char no2[delimOnwards] = {'\0'};
                    	
           strncpy(no1, string1, pos); //store in pointer to chars 
           strncpy(no2, returnV+1, originalSize);

           int num1 = atoi(no1); //conversion to int 
           int num2 = atoi(no2); 

           MultiRange(num1, num2);

         }else{
             int num = atoi(argv[i]);

         	MultiTable(num);
         }
	}
	
	return 0; 
}

void MultiRange(int start, int end){ //Times table with a range 
	

do{
	for(int i=0; i<13; i++){
		cout << start << " * " << i << " = " << start*i <<endl;
	}
	start++;

}while(start <= end);

cout << "\n";
}



void MultiTable(int num){ //Times table for a number 
	for(int i=0; i<13; i++){
		cout << num << " * " << i << " = " << num*i <<endl;
	}

	cout << "\n";
}
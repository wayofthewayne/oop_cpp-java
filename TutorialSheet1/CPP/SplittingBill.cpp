#include <iostream>
using namespace std; 

int Ways(int num, int upperLimit);

int main (){

    
    int combs = Ways(500,500);

    cout << "Total combinations: " <<combs;

    
	return 0;
}


int Ways(int num, int upperLimit){
       
       int ways = 0; 

       if(num >= 200 && 200 <= upperLimit){ // if number is bigger than 200, we can take away 200. We then recall the function, which will
       	 ways += Ways(num-200,200); //continue to this pattern of taking away a number till the number reaches 0; This means we have found a comb
       } // that reaches 500, so we return 1. Note this starts with finding the largest no s first i.e 200 200 100, 200 200 50 50 etc 

       if(num >= 100 && 100 <= upperLimit){ //uppper limit is used to avoid over counting (counting the same comb twice) i.e 2 2 1 and 2 1 2 
       	 ways += Ways(num-100,100);
       }

       if(num >= 50 && 50 <= upperLimit){
       	 ways += Ways(num-50,50);
       }


       if(num >= 20 && 20 <= upperLimit){
       	ways += Ways(num-20,20);
       }

       if(num >= 10 && 10 <= upperLimit){
       	ways += Ways(num-10,10);
       } 

       if(num >= 5 && 5 <= upperLimit){
       	 ways += Ways(num-5,5);
       }

       if(num >= 2 && 2 <= upperLimit){
       	 ways += Ways(num-2,2);
       }

       if(num >= 1 && 1 <= upperLimit){
       	 ways += Ways(num-1,1);
       }

       if(num ==0){
       	return 1; 
       }

       return ways; 
}

/* below find the teachers sol 


static int[] COINS = {1, 2, 5, 10, 20, 50, 100, 200};

public static int n_ways_split_rec(int change, int start_coin) {

        int ways = 0;

        if (start_coin >= COINS.length) return 0;

        var c = COINS[start_coin];

        for (var newChange = change; newChange >= 0; newChange -= c) {

            if (newChange == 0) ways += 1;

            else ways += n_ways_split_rec(newChange, start_coin + 1);

        }

        return ways;

    }

    */ 
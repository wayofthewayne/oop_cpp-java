#include <iostream>
#include <string>
using namespace std;

int charPts(char ch){

	char onePointLetters[]={'a','e','i','l','n','o','r','s','t','u'};

	// if given letter is found in the array above, return 1
	for(int i =0;i<10;i++){
		if(ch == onePointLetters[i]){
			return 1;
		}
	}
	char twoPointsLetters[]={'d','g'};

	// if given letter is found in the array above, return 2
	for(int i =0;i<2;i++){
		if(ch == twoPointsLetters[i]){
			return 2;
		}
	}

	char threePointsLetters[] ={'b','c','m','p'};

	// if given letter is found in the array above, return 3
	for(int i =0;i<4;i++){
		if(ch == threePointsLetters[i]){
			return 3;
		}
	}

	char fourPointsLetters[] ={'f','h','v','w','y'};

	// if given letter is found in the array above, return 4
	for(int i =0;i<5;i++){
		if(ch == fourPointsLetters[i]){
			return 4;
		}
	}


	if (ch == 'k'){
		return 5;
	}

	char eightPointsLetters[] = {'j','x'};

	// if given letter is found in the array above, return 8
	for(int i =0;i<2;i++){
		if(ch == eightPointsLetters[i]){
			return 8;
		}
	}

	char tenPointsLetters[] = {'q','z'};

	// if given letter is found in the array above, return 10
	for(int i =0;i<2;i++){
		if(ch == tenPointsLetters[i]){
			return 10;
		}
	}
	

	return 0;
	
}

int points(string word){

	int pts=0;

	for(unsigned long  i = 0; i<word.length();i++){ //adding up score 
		pts += charPts(word[i]);
	}

	return pts;

}


int main(void){

	string word;

	cin >> word;

	cout << "the word " << word << " contains " << points(word) << " points." << endl;
}

#include <iostream>
#include <string>
#include <stdlib.h>
#include <iomanip>
using namespace std; 


class Team{
public: //set to public since the print table function is outside the class and makes use of the variables 

	string Tname; 
	int points=0;
	int playedMatches=0;
	int goalsFor=0;
	int goalsAgainst=0; 
	int goalDifference=0; 
	int wins=0;
	int loses=0;
	int draws=0; 

public:

    Team(){   // this constructor is needed so we set the pointer to the array of teams      
     }

	Team(string name){ //Team constructor parameterized
       Tname = name; 
	}

	
	void Match(Team* away){ // we use a pointer to affect physical changes in the away team. Else we need a Team return type.
        int homeScore = (rand() % 5); //generating a random score from 0 to 5
        int awayScore = (rand() % 5);


        playedMatches++;
        away->playedMatches++; 

        goalsFor += homeScore;
        away->goalsFor += awayScore;

        goalsAgainst += awayScore;
        away->goalsAgainst += homeScore; 

        goalDifference = goalsFor - goalsAgainst;
        away->goalDifference = away->goalsFor - away->goalsAgainst;

        if(homeScore > awayScore){
        	wins++;
        	away->loses++;
        	points += 3;
        }else if (awayScore > homeScore){
        	away->wins++;
        	loses++; 
        	away->points +=3;
        }else{
        	draws++;
        	away->draws++;

        	points++;
        	away->points++; 
        }
        
	}
};


//we pass an array of teams 
void Print(Team teams[]){//will print the whole league table 
        cout << "Club           MP     W     D     L     GF     GA     GD     Pts" <<endl;
        cout << "----------------------------------------------------------------" <<endl;

        for(int i=0; i<20; i++){ //setw fixes coloum width. Here the algniment is not perfect 
            cout << setw(10) << left <<teams[i].Tname  << "     " << teams[i].playedMatches<< "     " << teams[i].wins << "    " << teams[i].draws<< 
            "     " << teams[i].loses<< "     " << teams[i].goalsFor<< "     " << teams[i].goalsAgainst<< "     " << teams[i].goalDifference<< 
            "     " << teams[i].points<<endl; 
        }
}


int main (){

   
    Team* teams = new Team[20]; //creating pointers to team array on Heap so we must delete eventually 

    string names[] = {"Juventus","Inter","Roma","Napoli","Atalanta","Milan","Sassuolo","Sampdoria","Verona",
		"Lazio","Benevento","Fiorentina","Cagliari","Spezia","Genoa","Udinese","Parma","Bologna","Torino","Crotone"};


    for(int i=0; i<20; i++){
        teams[i] = Team(names[i]); //setting team names
    }

    for(int i=0; i<19; i++){
        for(int j =i+1; j<20; j++){
            teams[i].Match(&teams[j]); //simulating matches 
            teams[j].Match(&teams[i]); //note how the addresses are passed 
        }
    }


        //sorting in descending order the Team objects according to the number of points they got.
        for (int i = 0; i < 20; i++){
            for (int j = i+1; j < 20; j++){
                if(teams[i].points <  teams[j].points){

                    Team temp = Team(); //creating temp team (we don t need a pointer in this case)

                    temp = teams[i]; //swapping 
                    teams[i]= teams[j];
                    teams[j]= temp;

                }
            }
        } 

//we pass an array of teams, as the parameter expects. 
    Print(teams); //printing tables    

    delete[] teams;//deleting the array of pointers to teams 

	return 0; 
}
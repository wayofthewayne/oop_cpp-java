#include <iostream>
using namespace std; 

#include <string.h>
#include <stdlib.h>
#include <time.h>


void Display(char board[]);
void Turns(char board[]);
bool ValidTurn(char board[], int input);
bool Win(char board[]);
void MENU();
void TurnsCPU(char board[]);

int main (void){
    char board[9] = {' ',' ',' ',' ',' ',' ',' ',' ',' '}; //initialsing an empty board 
    
    int option; 

    MENU();
    
    cin >> option;

    if(option == 1){
    	Turns(board);
    }else if (option == 2){
        TurnsCPU(board);
    }else{
    	exit(3);
    }



    

	return 0;
}


void Display(char board[]){ //will display the board 
      cout << "+-----+-----+" << endl; 
      cout << "| " << board[0] << " | " << board[1] << " | " << board[2] << " |" <<endl;
      cout << "| " << board[3] << " | " << board[4] << " | " << board[5] << " |" <<endl;
      cout << "| " << board[6] << " | " << board[7] << " | " << board[8] << " |" <<endl;
      cout << "+-----+-----+" << endl;
}

void Turns(char board[]){

	int input; 
	int turnCounter = 0; 

	for(;;){

	cout << "Player 1 turn..."; 
    cout << " Type position from 0 to 8"<<endl; 

    cin >> input;

    if (ValidTurn(board,input) == false){ //checking for valid input 
    	cout << "\n\nThis space is already occupied. Please try again" <<endl;
    	continue; // will ask player one to redo turn 
    }

    board[input] = 'X'; //setting input 

    Display(board); //showing board 

    if(Win(board) == true){ //checking for wins 
    	break;
    }


    turnCounter++;

    if(turnCounter == 9){ //checking for full board 
    	cout << "BOARD IS FULL!";
    	break;
    }

    do{
    cout << "Player 2 turn...";
    cout << " Type position from 0 to 8"<<endl; 

    cin >> input; 

    if (ValidTurn(board,input) == false){ //checking if valid turn 
    	cout << "\n\nThis space is already occupied. Please try again" <<endl;
    }

    }while(ValidTurn(board,input) == false); //will ask the player to re do turn 

    board[input] = 'O'; //setting input

    Display(board); //showing board 

    if(Win(board) == true){ //checking for wins 
    	break;
    }

    turnCounter++;

    if(turnCounter == 9){ //checking if board is full 
    	cout << "BOARD IS FULL!";
    	break;
    }

    }


} 

bool ValidTurn(char board[], int input){ //will check if turn valid by making sure the space is empty 
     if(board[input] == ' '){
     	return true;
     }else{
     	return false; 
     }
}

bool Win(char board[]){//will check for wins 

	for(int i=0; i<6; i+= 3){ //vertical win 
	   if(board[i] == board[i+1] && board[i+1] == board[i+2] && board[i] != ' '){
		  if(board[i]=='X'){
			cout<< "Player 1 wins"; 
			return true;
		  }else{
		  	cout<< "Player 2 wins";
		  	return true;
		  }
	   }
    }  

    for(int i=0; i<3; i++){ //horizontal win
	   if(board[i] == board[i+3] && board[i+3] == board[i+6] && board[i] != ' '){
		  if(board[i]=='X'){
			cout<< "Player 1 wins"; 
			return true;
		  }else{
		  	cout<< "Player 2 wins";
		  	return true;
		  }
	   }
    }   

    if(board[0] == board[4] && board[4]==board[8] && board[0] != ' '){ //diagonal wins 
    	if(board[0] == 'X'){
    		cout<< "Player 1 wins"; 
			return true;
		  }else{
		  	cout<< "Player 2 wins";
		  	return true;
    	}
    }

    if(board[2] == board[4] && board[4]==board[6] && board[2] != ' '){
    	if(board[2] == 'X'){
    		cout<< "Player 1 wins"; 
			return true;
		  }else{
		  	cout<< "Player 2 wins";
		  	return true;
    	}
    }

    return false; //no wins yet 
}

void MENU(){
	cout<< ".......TicTacToe Menu......."<<endl;
	cout<< "1. 2 Player mode"<<endl;
	cout<< "2. CPU mode"<<endl;
	cout<< "3. EXIT"<<endl;
	cout<< "\nPlease enter your option:"<<endl;

}

void TurnsCPU(char board[]){

	int input; 
	int turnCounter = 0; 

	for(;;){

	cout << "Player 1 turn..."; 
    cout << " Type position from 0 to 8"<<endl; 

    cin >> input;

    if (ValidTurn(board,input) == false){ //checking for valid input 
    	cout << "\n\nThis space is already occupied. Please try again" <<endl;
    	continue; // will ask player one to redo turn 
    }

    board[input] = 'X'; //setting input 

    Display(board); //showing board 

    if(Win(board) == true){ //checking for wins 
    	break;
    }


    turnCounter++;

    if(turnCounter == 9){ //checking for full board 
    	cout << "BOARD IS FULL!";
    	break;
    }
      
    cout << "CPU turn..."<<endl;

    do{ 
    
    input =  (rand()%8); //generating a random input for the CPU

    }while(ValidTurn(board,input) == false); //will re roll for cpu 

    board[input] = 'O'; //setting input

    Display(board); //showing board 

    if(Win(board) == true){ //checking for wins 
    	break;
    }

    turnCounter++;

    if(turnCounter == 9){ //checking if board is full 
    	cout << "BOARD IS FULL!";
    	break;
    }

    }


} 
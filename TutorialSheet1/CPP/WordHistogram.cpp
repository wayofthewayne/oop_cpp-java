#include <iostream>
#include <string>
#include <ctype.h>
#include <stdbool.h>
using namespace std;

int main(void){

	string storedStr;

	getline(cin,storedStr); //reading input



	for(unsigned long i = 0 ;i<storedStr.length() ;i++){

		bool alreadyCounted = false;

		if (isalpha(storedStr[i])==false){
			continue; //if char is not a letter, skip it.
		}

		int charCounter = 1;

		for(unsigned long j = 0; j<i; j++){ //array checking with previous chars in sentence to see if char was already counted 

			if(storedStr[j]==storedStr[i]){
				alreadyCounted=true;
				break;
			}
		}

		if(alreadyCounted){
			continue; // if letter is already catered for, skip it.
		}


		//determine how many instances of that letter
		for(unsigned long j = i+1; j < storedStr.length(); j++){

			if(storedStr[i]==storedStr[j]){
				charCounter++;
			}
		}


		//print the number of instances
		cout << storedStr[i] << "  ";

		for(int j = charCounter; j>0; j--){
			cout << "*";
		}

		cout << endl;

	}


	return 0;
}

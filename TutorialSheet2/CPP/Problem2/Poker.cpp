using namespace std; 
#include <string.h>
#include <stdlib.h>
#include <iostream>

class Card{

public:
	string rank; 
	string suite; 

	Card(){	
	}

	~Card(){
	}
   
};

class Deck{
public: 
	
	Card cardArr[52];  

	Deck(){


      	string cNo[] = {"Ace" ,"2","3","4","5","6","7","8","9","10","Jack","Queen","King"};

            for(int i=0; i<13; i++){ //filling up the deck with all the cards (giving them values)
                cardArr[i].rank.assign(cNo[i]);
                cardArr[i].suite.assign("Spades");
            }

            for(int i=0; i<13; i++){
            	cardArr[i+13].rank.assign(cNo[i]);
            	cardArr[i+13].suite.assign("Clubs");
            }

            for(int i=0; i<13; i++){
                cardArr[i+26].rank.assign(cNo[i]);
                cardArr[i+26].suite.assign("Hearts");
            }

            for(int i=0; i<13; i++){
            	cardArr[i+39].rank.assign(cNo[i]);
            	cardArr[i+39].suite.assign("Diamonds");
            }

      }

};

class Hand{
public: 

	Card pCardNo[5];

	Hand(){
	}

	bool isGoodHand(){//will determine if the hand is good in terms of the conditions specified in the tutorial sheet
      
      int counterForNo = 0; 
      int counterForSuite = 0;

      for(int i=0; i<5; i++){ //going through all the cards 
        for(int j=i+1; j<5; j++){

        	if(pCardNo[i].rank.compare(pCardNo[j].rank) == 0){ // if we find the same numbers 
                counterForNo++; //incremenet 
        	}

        	if(pCardNo[i].suite.compare(pCardNo[j].suite) == 0){//same concept for the suite 
                counterForSuite++;
        	}

        }
           if(counterForNo >= 1){ //i.e player at least has a pair (int terms of numbers) (may be btr than a pair)
             return true; 
           } 

           if(counterForSuite == 4){ //player must have hand all from same suite 
           	return true;
           }

        counterForNo = 0; //resetting counters 
        counterForSuite = 0; //note this is technically extra, since if the first suite is not like all the others, there is no chance for flush  
      }

      return false; //does not have a great hand 
    }

	~Hand(){
	}
};

class Croupier{
public:

	Deck gameDeck = Deck(); 

	Hand Deal(){
		Hand pHand = Hand();

		int random = 0;

		for(int i=0; i<5; i++){
			random = rand()%52;

			if(gameDeck.cardArr[random].rank.compare("used") != 0){ //checking if card was handed already 

          pHand.pCardNo[i].rank.assign(gameDeck.cardArr[random].rank); // setting the hand for player 
          pHand.pCardNo[i].suite.assign(gameDeck.cardArr[random].suite); 
          //in the case of strings, java creates a new string (even tho it is a reference type) (strings are immutable)

          gameDeck.cardArr[random].rank.assign("used"); // flagging cards in use  

          }else{
          	i--; //reset counter 
          }

		}
      
         return pHand;

	}

	void Shuffle(){ 
	   gameDeck = Deck();
	}

};

class Player{//note in player we make use of setters and getters simply to use them 

	private: //it allows us to set this attribute on private 

	string name; // a player will have a name and a hand 

public: 
	Hand phand = Hand(); 

	void SetName(string Passedname){ // then use this public function to set the name
		name = Passedname; 
	}
	string GetName(){ //and this function to retrieve the name
		return name; 
	}

};


int main (int argc, char **argv){ //Refer to java for comments 
	Croupier dealer = Croupier(); 
    

   if(argc != 3){
    	cout << "You have enetered insufficient arguements!"<<endl;
    }


    long int numberOfPlayers = strtol(argv[1],NULL,10);
    long int numberOfMatches = strtol(argv[2], NULL,10);

    Player players[numberOfPlayers];
    string playerNames[numberOfPlayers]; 

    for(int i=0; i<numberOfPlayers; i++){
    	cout << "Enter name for player: " << i+1<<endl;
    	cin >> playerNames[i];

    	players[i].SetName(playerNames[i]);
    }

    int counter = 0; 

    cout << "\n\n"<<endl; 

    do{

     cout << "Start of match: " << counter+1 << endl; 

     for(int i=0; i<numberOfPlayers; i++){
     	players[i].phand = dealer.Deal(); 

     	cout << players[i].GetName() << ":" << endl; 

     	for(int j=0; j<5; j++){
     		cout << players[i].phand.pCardNo[j].rank << " " << players[i].phand.pCardNo[j].suite <<endl; 
     	}
     	cout << "\n";
     }

     dealer.Shuffle(); 
     counter ++; 

    }while(counter < numberOfMatches);

    dealer.Shuffle(); 

    Hand test = Hand(); 
    test = dealer.Deal();
    bool returnVal = test.isGoodHand();

    if(returnVal == true){
    	cout <<"Good Hand"<<endl;
    }else{
    	cout <<"Bad Hand"<<endl; 
    }

}
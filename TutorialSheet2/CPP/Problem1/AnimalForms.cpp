#include "Animal.h" 

void Snake::Slither(){ //defining snake function 
    cout << name << " is slithering..." << endl; 
}

void Snake::Vaccinate(string disease){ //functions from pet 
	cout << name << " the snake has been vaccinated from " << disease << endl;
}

void Snake::CelebrateMaster(){
	cout << name << " is celebrating his master" << endl; 
}

void Snake::AttackHuman(){//functions from dangerous animal 
	cout << name << " has bit human with venomous fangs" << endl; 
}



void Tiger::Roar(){ //tiger function 
   cout << name << " said RRRRRRROOOOOAAAAAARRRRRRR!!!" << endl; 
}

void Tiger::AttackHuman(){ //dangerousanimal function 
	cout << name << " has slashed human to death" << endl; 
}



void Goldfish::Swim(){ //fish function 
	cout << name << " has swam" << endl; 
}

void Goldfish::Vaccinate(string disease){ //pet functions 
	cout << name << " the goldfish has been vaccinated from " << disease <<endl; 
}

void Goldfish::CelebrateMaster(){
	cout << name << " is celebrating its owner." << endl;
}



void Donkey::Eatgrass(string grassType){ //donkey function 
	cout << name << " is eating " << grassType << endl; 
}

void Donkey::Vaccinate(string disease){ //pet functions 
	cout << name << " the donkey has been vaccinated from " << disease << endl; 
}

void Donkey::CelebrateMaster(){
	cout << name << " is cuddling with his master." << endl; 
}



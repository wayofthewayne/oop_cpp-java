#include "Animal.h"
#include <type_traits>

class Vet{ //vet class with it s function signatures 
  public: 
  	void Vaccinate (Pet somePet, string someDisease); 
  	bool Examine (Animal * someAnimal);
};
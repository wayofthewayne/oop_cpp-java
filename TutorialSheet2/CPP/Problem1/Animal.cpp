#include "Animal.h"

Animal::Animal(){ //animal defualt constructor definition 
}

Animal::Animal(string Animalname){ // parameterised constructor definition so we can set name 
	name = Animalname;
	cout << "Animal with name: " << name << " was created" <<endl; 
}

Animal::~Animal(){ //destroctuor definition (this will let us know when the object was destroyed) regardless if pointer form or not was used 
	cout << endl <<name << " was destroyed."  << endl << endl; 
}

void Animal::Eat(string food){ //defining the animal function eat.
     cout << name << " is eating " << food << endl;  
}
#include <iostream>
using namespace std; 

class Animal{ //declaring animal class
  public: 
     string name; //this must be public since it will be used in main 
     //Could have used getters and setters to set it private 

  public:   //functions must be public 
    
     Animal(); //default constructor (so we can create animal types before setting name)
     Animal(string animalName); //parameterised constructor so we can set the name 
     virtual ~Animal(); //destructor (this is set to virtual since it will need to ovverriden when we delete s2)
     //this is required for polymorphism 

     void Eat(string food); //animal functions 
     virtual bool isDangerous(){ return false; }; //all animals initially set to non dangerous
};


class Pet{ // pet class 
    public: 
    	virtual void Vaccinate(string disease) { cout << "Pet has been vaccinated from " << disease <<endl; }; //setting virtual functions so they can be overwriten 
    	//the one above is defined so we can use it when for a pet when called from Vet 
    	//it is then redefined in snake, tiger etc etc. This is so they have their own personilsed Vaccinate function 
    	
    	virtual void CelebrateMaster(){}; //virtual function not used in pet 
};

class DangerousAnimal: public Animal{
  public:
  	virtual void AttackHuman(){}; //virtual function to be overriden in all the other sub classes of it 
    bool isDangerous(){ return true; } //all animals of this class are set to dangeriys 
};

//note for the rest of the classes since constructors and destructors were not explicity set they will be set automatically by the compliler 

class Snake: public Pet, public DangerousAnimal{ //snake inheriting from multiple classes (since dangerous Animal inherits from animal, snake does not need to inherit from animal)
public:
    void Slither(); //snake function 
    void Vaccinate(string disease); //functions from pet 
    void CelebrateMaster();
    void AttackHuman(); //function from dangerous animal that is overriden 
};

class Tiger: public DangerousAnimal{ //tiger class inheriting from mutliple classes
public:
	void Roar(); // tiger function 
	void AttackHuman(); //function from dangerous animal 
};

class Goldfish: public Animal, public Pet{ //goldfish class inheriting from animal and pet 
public:
	void Swim(); //goldfish function 
	void Vaccinate(string disease); //pet functions 
	void CelebrateMaster(); 
};

class Donkey: public Animal, public Pet{ //donkey class inheriting from 2 classes
public: 
	void Eatgrass(string grassType); //donkey class 
	void Vaccinate(string disease); //pet functions 
	void CelebrateMaster();
};


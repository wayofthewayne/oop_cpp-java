#include "Vet.h" //automatically includes Animal.h since this is included in Vet.h


int main (void){
	Snake s1 = Snake(); //declaring a snake 
    s1.name = "snakey";

    Vet drDoLittle = Vet(); //declaring vet 

    drDoLittle.Vaccinate(s1,"malaria"); //vet function 

    cout << endl; 

    Snake *s2 = new Snake(); //creating a pointer to snake so it can be passed to the function 
    s2->name = "drakey";

    Donkey *d2 = new Donkey();
    d2->name = "omar";

    drDoLittle.Examine(s2);//another vet function 
    drDoLittle.Examine(d2);

    delete s2; //we need a virtual destructor in the super class to perform this 
    delete d2;


    s1.Eat("apple");  //testing animal function on snake 
	s1.Slither(); //snake function on snake 

	s1.Vaccinate("aids"); //more functions 
	s1.AttackHuman(); 
	return 0; 

}
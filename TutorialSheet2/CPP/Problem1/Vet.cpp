#include "Vet.h"


void Vet::Vaccinate(Pet somePet, string disease){ //Vet vaccinate will call the pet vaccinate (the virtual version)
    somePet.Vaccinate(disease);
}

bool Vet::Examine(Animal * someAnimal){ //return type is redudant with function definition however asked for in the question 
	if(someAnimal->isDangerous() == false){ //determine if the animal is dangerous 

		cout << someAnimal->name << " has been examined." <<endl;
		return true;
	}else{
		DangerousAnimal *pt = dynamic_cast<DangerousAnimal*>(someAnimal); // (if it is cast to dangerousAnimal type)
		if(pt !=NULL){
			pt->AttackHuman(); //dangerousAnimal attacks human 
		}
		cout << "Examination Failed!" << endl; 
		return false; 
	}
}
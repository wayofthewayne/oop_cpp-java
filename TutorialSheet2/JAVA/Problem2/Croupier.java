import java.util.Random; 

public class Croupier{
     
    Deck gameDeck = new Deck(); //will create a deck once we create a croupier 

      
	public Hand Deal(){ //Will fill up a hand with cards from deck 
		 
		Hand phand = new Hand();// Create a hand 

        Random random = new Random(); 
        int rand=0; 

        for(int i=0; i<5; ++i){
          rand = random.nextInt(52); //generating random no between 0-51; 
          
          if((gameDeck.cardArr[rand].no.equals("used") ) != true){ //checking if card was handed already 

          //phand.hand[i] = gameDeck.cardArr[rand]; if we tried equating this line for cards, we would change reference to of hand to deck, so we get suiteused
          //note this in turn will make hand point to gameDeck which has no set to used, so hand will also point there 



          phand.hand[i].no = gameDeck.cardArr[rand].no; // setting the hand for player 
          phand.hand[i].suite = gameDeck.cardArr[rand].suite; 
          //in the case of strings, java creates a new string (even tho it is a reference type) (strings are immutable)

          gameDeck.cardArr[rand].no = "used"; // flagging cards in use  

          }else{
          	i--; //reset counter 
          }
       }

        return phand; //returning hand 
	}

	public void Shuffle(){ //will allow to reshuffle and refill up deck 
		
         gameDeck = null; // freeing memory from gamedeck (probably irrelavent)
         gameDeck = new Deck();  //reset the deck 

	}

}
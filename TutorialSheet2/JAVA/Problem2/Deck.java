public class Deck{

	  Card cardArr[] = new Card[52]; //we define a deck as 52 cards 
      
        Deck(){ //When we create a deck 


        for(int i=0; i<52; i++){
      		cardArr[i] = new Card(); //we initialise all the cards 
      	}


      	String cNo[] = {"Ace" ,"2","3","4","5","6","7","8","9","10","Jack","Queen","King"};

            for(int i=0; i<13; i++){ //filling up the deck with all the cards (giving them values)
                cardArr[i].no = cNo[i];
                cardArr[i].suite = "Spades";
            }

            for(int i=0; i<13; i++){
            	cardArr[i+13].no = cNo[i];
            	cardArr[i+13].suite = "Clubs";
            }

            for(int i=0; i<13; i++){
                cardArr[i+26].no = cNo[i];
                cardArr[i+26].suite = "Hearts";
            }

            for(int i=0; i<13; i++){
            	cardArr[i+39].no = cNo[i];
            	cardArr[i+39].suite = "Diamonds";
            }

      }
}
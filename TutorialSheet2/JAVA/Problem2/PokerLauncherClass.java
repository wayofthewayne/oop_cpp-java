import java.util.Scanner; 

public class PokerLauncherClass{
	public static void main (String args []){

         Scanner scan = new Scanner(System.in);

           Croupier dealer = new Croupier(); //creating dealer type 
           
           if(args.length != 2){ //making sure players and matches are passed 
           	 System.out.println("Invalid Arguments. Please enter amount of players playing, and after the amount of matches ");
           }

           int playerAmount = Integer.parseInt(args[0]); //matches and players are typecasted from string to int 
           int matches = Integer.parseInt(args[1]);

           String playerNames[] = new String[playerAmount];  //will store player names 
           Player players[] = new Player[playerAmount]; //will store the player types 

           int counter =0; 

           for(int i=0; i<playerAmount; i++){//for all the players 

           	System.out.println("Enter name of player: " + (i+1));
              playerNames[i] = scan.nextLine();//read their names 

             players[i] = new Player(); //initialise the player
           	 players[i].name = playerNames[i];//set thier name 
           }



           //BELOW IS THE TESTING FOR isGoodHand() !!! 

          /*for(int i=0; i<5; i++){ //the following code checks for a flush (manually)
           	players[0].phand.hand[i].suite = "Clubs";
           }

           players[0].phand.hand[0].no = "Ace";
           players[0].phand.hand[1].no = "Two";
           players[0].phand.hand[2].no = "Three";
           players[0].phand.hand[3].no = "Four";


           boolean checkHand = players[0].phand.isGoodHand();

           if(checkHand == true){
           	 System.out.println("Player has a good hand"); 
           }else{
           	System.out.println("Bad hand");
           }*/

           System.out.println("\n\n");

           do{

           	System.out.println("Start of match: " + (counter+1) ); //match counter displayed

           for(int i=0; i<playerAmount; i++){ //for all the players 

           	 players[i].phand = dealer.Deal(); //Croupier will call which returns a hand which is stored in the player hand 
           	 System.out.println(players[i].name + ":\n"); //player name is displayed 

           	 for(int j=0; j<5; j++){ //along with their cards 
           	 	System.out.println(players[i].phand.hand[j].suite +"-"+ players[i].phand.hand[j].no);
           	 }
           	 System.out.println("\n");
           }

             
           dealer.Shuffle(); //start of new match
           counter++; //match counter incremented 

       }while(counter < matches); //this is repeated for all the matches 
            
	}
}
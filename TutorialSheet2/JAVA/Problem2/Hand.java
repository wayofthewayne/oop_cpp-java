public class Hand{

    Card hand[] = new Card[5]; //A hand will contain 5 cards (next time call hand card for a btr naming convention)

     Hand(){
    	for(int i=0; i<5; i++){
    		hand[i] = new Card();  //constructor will initialise the 5 cards 
    	}
    }

    public boolean isGoodHand(){//will determine if the hand is good in terms of the conditions specified in the tutorial sheet
      
      int counterForNo = 0; 
      int counterForSuite = 0;

      for(int i=0; i<5; i++){ //going through all the cards 
        for(int j=i+1; j<5; j++){

        	if(hand[i].no.equals(hand[j].no) == true){ // if we find the same numbers 
                counterForNo++; //incremenet 
        	}

        	if(hand[i].suite.equals(hand[j].suite) == true){//same concept for the suite 
                counterForSuite++;
        	}

        }
           if(counterForNo >= 1){ //i.e player at least has a pair (int terms of numbers) (may be btr than a pair)
             return true; 
           } 

           if(counterForSuite == 4){ //player must have hand all from same suite 
           	return true;
           }

        counterForNo = 0; //resetting counters 
        counterForSuite = 0; //note this is technically extra, since if the first suite is not like all the others, there is no chance for flush  
      }

      return false; //does not have a great hand 
    }

 }
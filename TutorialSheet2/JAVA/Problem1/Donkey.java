import java.util.Random; 

public class Donkey extends Animal implements Pet{ //another sub class being a pet 
	
	public void IsHungry(){ //donkey function 
		Random rand = new Random(); 
	    int hungry = rand.nextInt(2); 

	    if(hungry == 1){
	    	System.out.println("The donkey " + name + " is hungry");
	    }else{
	    	System.out.println("The donkey " + name + " is not hungry");
	    }
	}

	public void Vaccinate(String disease){ //pet functions 
		System.out.println("The donkey has been vaccinated from " + disease); 
	}

	public void CelebrateMaster(){
		System.out.println(name + " is celebrating his owner");
	}
}
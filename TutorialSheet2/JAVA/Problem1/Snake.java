public class Snake extends Animal implements Pet, DangerousAnimal{ //a sub class both a pet and dangerous animal 

    

	public void IsVenomous(String venomProfile){ //function related to snake 
       String confirmation = "YES";
       String venom = venomProfile.toUpperCase();

		if(confirmation.equals(venom)){
                 System.out.println(name + "is venomous");
		}else{
				 System.out.println(name + "is not venomous");	
		}
	}

	public void Vaccinate(String disease){// Pet related functions 
		System.out.println("The snake " + name +" has been vaccinated from " + disease); 
	}

	public void CelebrateMaster(){
		System.out.println(name + " the snake is proud of its owner");
	}
    
    public void AttackHuman(){ // Dangerous Animal related functions 
    	System.out.println("The snake is attacking the human"); 
    }

}
public class Tiger extends Animal implements DangerousAnimal { //another subclass being a dangerour animal 
	
	public void Roar(String roarSound){ // tiger related function 
		System.out.println("The tiger named " + name + "has said " + roarSound); 
	}

	public void AttackHuman(){ //dangerous animal function 
		System.out.println("Tiger attacking ....."); 
	}
}
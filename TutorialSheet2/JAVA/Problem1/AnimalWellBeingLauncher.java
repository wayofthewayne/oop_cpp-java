public class AnimalWellBeingLauncher { //This is the main class running everything and making use of the functionality
	public static void main(String [] args){

           Vet drDoLittle = new Vet(); //creating instance of Vet 

           Snake snake1 = new Snake();  //Instance of snake and giving a name 
           snake1.name = "snakey";

           Donkey donkey1 = new Donkey(); //Instance of donkey and giving name
           donkey1.name = "omar";  //note if for example we do not give the donkey a name name is set as null 

           Tiger tiger1 = new Tiger(); //instance of tiger
           tiger1.name = "ginger";



           tiger1.Roar("ROOOOOAAAARRR!!!"); //tiger specific function

           donkey1.IsHungry(); //running is hungry function for donkey (specific to donkey)
           donkey1.Eat("apple"); //Attempting to call function from animal on a donkey instance


           drDoLittle.Vaccinate(snake1, "malaria"); //vaccinating snake (will use vet vaccinate function to call snake vaccinate function)


           boolean examinationSuccesfulSnake = true; 
           boolean examinationSuccesfulDonkey = false; 

           examinationSuccesfulSnake = drDoLittle.Examine(snake1); //Vet examining snake (a dangerous animal)          

           if(examinationSuccesfulSnake == false){ //output determined by return type of Examine (related to what type of animal is passed)
           	System.out.println("Examination failed");
           }else{
           	System.out.println("Examination Succesful");
           }



           examinationSuccesfulDonkey = drDoLittle.Examine(donkey1); //vet examining a donkey (pet)

           if(examinationSuccesfulDonkey == false){ //output determined by return type of Examine (related to what type of animal is passed)
           	System.out.println("Examination failed");
           }else{
           	System.out.println("Examination Succesful");
           }



           
	}
}
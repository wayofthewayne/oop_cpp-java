public class Animal{//This is the super class
	
	public String name; // name attribute

	public void Eat(String food){  //Functions related to animal type 
		System.out.println(name + "has eaten: " + food); 
	}

	public void Drink(String liquid){
		System.out.println(name + "has drank: " + liquid); 
	}
}
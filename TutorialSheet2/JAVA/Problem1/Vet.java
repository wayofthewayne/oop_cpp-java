public class Vet{ //vet class

	public void Vaccinate(Pet somePet, String disease){ //vet will vaccinate any pet type 
		somePet.Vaccinate(disease); //pet will call the respective vaccinate function in it s class
	}
    
    public boolean Examine(Animal someAnimal){ //vet will examine animal 

    	System.out.println("Examing " + someAnimal.name + "\n"); //output which animal is being examined  

              if(someAnimal instanceof DangerousAnimal){ //if animal is dangerous, it will attack human and fail examination 

              ((DangerousAnimal)someAnimal).AttackHuman(); //typecasting animal into a dangerous animal where attackhuman is defined. 
              	return false;
              }else{
              	return true; 
              }
              //will return if successful or not 
    }
    
}
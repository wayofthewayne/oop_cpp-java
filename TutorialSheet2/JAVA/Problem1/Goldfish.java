public class Goldfish extends Animal implements Pet{ //sub class being a pet 
	
	public void Swim(int speed){ //fish function 
         System.out.println("Goldfish is swimming at a speed of " + speed); 
	}

	public void Vaccinate(String disease){ // functions related to pet 
        System.out.println("This goldfish has been vaccinated from " + disease); 
	}

	public void CelebrateMaster(){
		System.out.println(name + " the goldfish is happy with it's owner" );
	}
}